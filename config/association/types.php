<?php

use Doctrine\DBAL\Types\Types as DBTypes;
use Synergyhub\DocsGenerator\Enums\OpenApi\Types as SwaggerTypes;

return [
    SwaggerTypes::STRING => [
        DBTypes::STRING,
        DBTypes::ASCII_STRING,
        DBTypes::TEXT,
        DBTypes::DATE_MUTABLE,
        DBTypes::DATE_IMMUTABLE ,
        DBTypes::DATEINTERVAL ,
        DBTypes::DATETIME_MUTABLE ,
        DBTypes::DATETIME_IMMUTABLE,
        DBTypes::DATETIMETZ_MUTABLE ,
        DBTypes::DATETIMETZ_IMMUTABLE,
        DBTypes::TIME_MUTABLE,
        DBTypes::TIME_IMMUTABLE,
        DBTypes::GUID,
        DBTypes::BLOB,
        DBTypes::BINARY,
    ],
    SwaggerTypes::NUMBER => [
        DBTypes::FLOAT,
        DBTypes::DECIMAL,
    ],
    SwaggerTypes::INTEGER => [
        DBTypes::INTEGER,
        DBTypes::BIGINT,
        DBTypes::SMALLINT,
    ],
    SwaggerTypes::BOOLEAN => [
        DBTypes::BOOLEAN,
    ],
    SwaggerTypes::ARRAY => [
        DBTypes::ARRAY,
        DBTypes::SIMPLE_ARRAY,
    ],
    SwaggerTypes::OBJECT => [
        DBTypes::JSON,
        DBTypes::OBJECT,
    ],
];
