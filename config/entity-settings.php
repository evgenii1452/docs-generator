<?php

/**
 * -----------------------------------------------------------
 *
 * В этом файле содержатся настройки для сущностей
 *
 * Ключом служат имена классов моделей с полным неймспейсом.
 * Значение - массив
 * В примере описана структура необходимого массива.
 *
 * -----------------------------------------------------------
 */

return [
//    'App/Models/Product' => [
//        'include' => [
//            'persons',
//            'levels',
//            'formats',
//            'productPlaces',
//            'category',
//            'organization',
//            'productType',
//            'entitySections',
//            'offers',
//            'seoTag'
//        ],
//        'filters' => [
//            'id' => ['type' => 'integer'],
//            'slug' => ['type' => 'string'],
//            'organization_ids' => ['type' => 'integer'],
//            'organization-slug' => ['type' => 'integer'],
//            'published' => ['type' => 'boolean']
//        ],
//        'sort' => ['id', '-id']
//    ],
];
