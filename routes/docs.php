<?php

$configs = config('docs-generator.settings');

Route::get($configs['docs_url'], function () use ($configs) {
    return view('redoc.spec');
})->name('docs-generator.spec');

Route::get($configs['document_url'], function () use ($configs) {
    return File::get(sprintf('%s/openapi.yaml', $configs['docs_path']));
})->name('docs-generator.spec.document');
