<?php

declare(strict_types=1);

namespace Synergyhub\DocsGenerator\Commands\Builders;

use Illuminate\Console\Command;
use Symfony\Component\CssSelector\Exception\ParseException;
use Symfony\Component\Yaml\Yaml;
use Synergyhub\DocsGenerator\Enums\CommandNames;

final class OpenApiBuildCommand extends Command
{
    protected $signature = CommandNames::DOCS_GENERATE_BUILD;

    protected $description = 'Collects dock totals from sources in the spec/folder in .yaml format';

    private string $srcPath;
    private string $openApiFilePath;
    private string $buildFilePath;

    public function __construct()
    {
        parent::__construct();

        $this->srcPath = config('docs-generator.settings.docs_path') . '/src';
        $this->buildFilePath = config('docs-generator.settings.docs_path') . '/src/build.yaml';
        $this->openApiFilePath = config('docs-generator.settings.docs_path') . '/openapi.yaml';
    }

    public function handle(): void
    {
        $fileName = $this->build();

        $this->info('Final documentation file ' . $fileName . ' successfully build!');
    }

    public function build(): string
    {
        $openApiFile = $this->parseYamlFile($this->buildFilePath);

        foreach ($openApiFile['paths'] ?? [] as $key => $path) {
            $openApiFile['paths'][$key] = $this->parseYamlFile("{$this->srcPath}/$path");
        }

        foreach ($openApiFile['components'] as $type => $componentsPaths) {
            $components = [];
            foreach ($componentsPaths as $componentPath) {
                $components = array_merge($components, $this->parseYamlFile("/{$this->srcPath}/$componentPath"));
            }
            $openApiFile['components'][$type] = $components;
        }

        return $this->buildOpenApiFile($openApiFile);
    }

    private function parseYamlFile(string $path)
    {
        try {
            return Yaml::parseFile($path);
        } catch (ParseException $exception) {
            printf('Unable to parse the YAML string: %s', $exception->getMessage());
        }
    }

    private function buildOpenApiFile(array $openApiFile): string
    {
        $openApiFile = Yaml::dump($openApiFile, 2, 2);

        file_put_contents($this->openApiFilePath, $openApiFile);

        return $this->openApiFilePath;
    }


}
