<?php

namespace Synergyhub\DocsGenerator\Commands;

use Illuminate\Support\Facades\DB;
use Illuminate\Console\Command;
use Synergyhub\DocsGenerator\Util\BuildUpdater;

class DocsBuildCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'docs:build-update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Генерация schema для openapi документации';


    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        $buildUpdater = new BuildUpdater();
        $buildUpdater->update();
    }
}
