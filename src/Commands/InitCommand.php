<?php

namespace Synergyhub\DocsGenerator\Commands;

use Illuminate\Support\Facades\DB;
use Illuminate\Console\Command;
use Synergyhub\DocsGenerator\Parsers\ParametersParser;
use Synergyhub\DocsGenerator\Util\BuildUpdater;
use Synergyhub\DocsGenerator\Util\FileSaver;

class InitCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'docs:init';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Инициализация';

    public function handle()
    {
    }
}
