<?php

namespace Synergyhub\DocsGenerator\Commands\Generators\Response;

use Illuminate\Console\Command;
use Illuminate\Support\Arr;
use ReflectionClass;
use Synergyhub\DocsGenerator\Commands\Generators\AbstractGenerateCommand;
use Synergyhub\DocsGenerator\Enums\Actions;
use Synergyhub\DocsGenerator\Enums\CommandNames;
use Synergyhub\DocsGenerator\Enums\ResponseTypes;
use Synergyhub\DocsGenerator\Generators\Response\ResponseGenerator;
use Synergyhub\DocsGenerator\Util\PathStorage;
use Synergyhub\DocsGenerator\Util\FileSaver;
use Synergyhub\DocsGenerator\Parsers\ModelsParser;

class ResponseGenerateCommand extends AbstractGenerateCommand
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = CommandNames::DOCS_GENERATE_RESPONSE . ' {entity?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Генерация компонентов "response" для openapi документации';

    public function handle()
    {
        $entity = $this->getEntity();
        $entityName = (new ReflectionClass($entity))->getShortName();

        $types = array_values((new ReflectionClass(ResponseTypes::class))->getConstants());
        $selectedType = $this->choice('Выберите из тип:', $types, 0);


        if ($selectedType == ResponseTypes::SKIP) {
            return;
        }

        if ($selectedType == ResponseTypes::ALL) {

            foreach ($types as $type) {
                if (!in_array($type, [ResponseTypes::SKIP, ResponseTypes::ALL])) {
                    $this->generateResponse($entityName, $type);
                }
            }

            return;
        }

        $this->generateResponse($entityName, $selectedType);

        if ($this->confirm('Создать Response с другим типом?', true)) {
            $this->runCommand(CommandNames::DOCS_GENERATE_RESPONSE, $this->arguments(), $this->output);
        }
    }

    private function generateResponse(string $entityName, string $action)
    {
        try {
            $data = (new ResponseGenerator())->generate($this->getEntity(), $action);

            if (in_array($action, [ResponseTypes::RELATED, ResponseTypes::RELATIONSHIPS])) {
                $dirPath = $this->ask("Название директории:", PathStorage::getResponseRelatedDir($entityName));
                $filename = $this->ask("Название файла:", PathStorage::getResponseRelationshipsFilename($entityName, $action));
            } else {
                $dirPath = $this->ask("Название директории:", PathStorage::getResponseDir($entityName));
                $filename = $this->ask("Название файла:", PathStorage::getResponseFilename($entityName, $action));
            }

            FileSaver::saveYamlFromArray($data, $dirPath, $filename);
        } catch (\Exception $exception) {
            echo $exception->getMessage();

            return;
        }
    }


}
