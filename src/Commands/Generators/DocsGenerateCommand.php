<?php

namespace Synergyhub\DocsGenerator\Commands\Generators;

use Illuminate\Console\Command;
use Illuminate\Database\Eloquent\Model;
use ReflectionClass;
use Synergyhub\DocsGenerator\Enums\CommandNames;
use Synergyhub\DocsGenerator\Parsers\ParametersParser;
use Synergyhub\DocsGenerator\Parsers\ModelsParser;

class DocsGenerateCommand extends AbstractGenerateCommand
{
    protected $signature = CommandNames::DOCS_GENERATE_COMPONENTS  . ' {entity?} {--all}';

    protected $description = 'Генерация всех компонентов для документация по спецификации jsonapi';

    public function handle()
    {
        if ($this->option('all')) {
            $this->generateAll();

            return;
        }

        $this->getEntity();

        $this->selectMode();

        $this->runCommand(CommandNames::DOCS_GENERATE_SCHEMA, $this->arguments(), $this->output);
        $this->runCommand(CommandNames::DOCS_GENERATE_PARAMS, $this->arguments(), $this->output);
        $this->runCommand(CommandNames::DOCS_GENERATE_REQUEST, $this->arguments(), $this->output);
        $this->runCommand(CommandNames::DOCS_GENERATE_RESPONSE, $this->arguments(), $this->output);
    }

    private function generateAll()
    {
        $this->selectMode();

        $entities = config('docs-generator.entity-settings');

        foreach ($entities as $entityClass => $configs) {
            $this->info("\n------- $entityClass -------\n");
            $this->input->setArgument('entity', new $entityClass());

            $this->runCommand(CommandNames::DOCS_GENERATE_SCHEMA, $this->arguments(), $this->output);
            $this->runCommand(CommandNames::DOCS_GENERATE_PARAMS, $this->arguments(), $this->output);
            $this->runCommand(CommandNames::DOCS_GENERATE_REQUEST, $this->arguments(), $this->output);
            $this->runCommand(CommandNames::DOCS_GENERATE_RESPONSE, $this->arguments(), $this->output);
        }

    }
}
