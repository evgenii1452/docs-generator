<?php

namespace Synergyhub\DocsGenerator\Commands\Generators\Parameters;

use Illuminate\Console\Command;
use PHPUnit\Exception;
use ReflectionClass;
use Synergyhub\DocsGenerator\Commands\Generators\AbstractGenerateCommand;
use Synergyhub\DocsGenerator\Enums\CommandNames;
use Synergyhub\DocsGenerator\Generators\Parameters\EntityParametersGenerator;
use Synergyhub\DocsGenerator\Generators\Parameters\FilterParametersGenerator;
use Synergyhub\DocsGenerator\Util\PathStorage;
use Synergyhub\DocsGenerator\Util\FileSaver;
use Synergyhub\DocsGenerator\Parsers\ModelsParser;

class EntityParametersGenerateCommand extends AbstractGenerateCommand
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = CommandNames::DOCS_GENERATE_PARAMS . ' {entity?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Генерация компонентов "parameters" для openapi документации';


    public function handle()
    {
        $entity = $this->getEntity();
        $entityName = (new ReflectionClass($entity))->getShortName();

        if ($this->confirm("Создать компонент EntityParameters?", true)) {
            $this->generateEntityParameters($entityName);
        }

        if ($this->confirm("Создать компонент FilterParameters?", true)) {
            $this->generateFilterParameters($entityName);
        }
    }

    private function generateEntityParameters(string $entityName)
    {
        $data = (new EntityParametersGenerator())->generate($this->getEntity());
        $dirPath = $this->ask("Название директории:", PathStorage::getParametersDir($entityName));
        $filename = $this->ask("Название файла:", PathStorage::getEntityParametersFilename($entityName));

        FileSaver::saveYamlFromArray($data, $dirPath, $filename);
        $this->info("File {$filename} created successfully");
    }

    private function generateFilterParameters(string $entityName)
    {
        try {
            $data = (new FilterParametersGenerator())->generate($this->getEntity());
            $dirPath = $this->ask("Название директории:", PathStorage::getParametersDir($entityName));
            $filename = $this->ask("Название файла:", PathStorage::getEntityFilterParametersFilename($entityName));

            FileSaver::saveYamlFromArray($data, $dirPath, $filename);
            $this->info("File {$filename} created successfully");
        } catch (\Exception $exception) {
            echo $exception->getMessage();

            return;
        }
    }
}
