<?php

namespace Synergyhub\DocsGenerator\Commands\Generators;

use Illuminate\Console\Command;
use Illuminate\Database\Eloquent\Model;
use Synergyhub\DocsGenerator\Parsers\ModelsParser;

abstract class AbstractGenerateCommand extends Command
{
    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    protected function getEntity(): Model|null
    {
        if (!is_null($this->argument('entity'))) {
            return $this->argument('entity');
        }

        $models = $this->getModels();
        $class = $this->anticipate('Enter Model name', array_keys($models));
        $this->input->setArgument('entity', new $models[$class]());

        return $this->argument('entity');
    }

    protected function getModels(): array
    {
        return (new ModelsParser)->getModels();
    }

    protected function selectMode()
    {
        if (!$this->confirm('Включить интерактивный режим?', false)) {
            $this->input->setOption('no-interaction', true);
        }
    }
}
