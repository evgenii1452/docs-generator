<?php

namespace Synergyhub\DocsGenerator\Commands\Generators;

use ReflectionClass;
use Synergyhub\DocsGenerator\Enums\CommandNames;
use Synergyhub\DocsGenerator\Generators\Path\EntitiesIdPathGenerator;
use Synergyhub\DocsGenerator\Generators\Path\EntitiesIdRelatedPathGenerator;
use Synergyhub\DocsGenerator\Generators\Path\EntitiesIdRelationshipsPathGenerator;
use Synergyhub\DocsGenerator\Generators\Path\EntitiesPathGenerator;
use Synergyhub\DocsGenerator\Parsers\ParametersParser;
use Synergyhub\DocsGenerator\Util\FileSaver;
use Synergyhub\DocsGenerator\Util\Helper;
use Synergyhub\DocsGenerator\Util\PathStorage;
use Synergyhub\DocsGenerator\Util\SettingsGetter;

class PathGenerateCommand extends AbstractGenerateCommand
{
    protected $signature = CommandNames::DOCS_GENERATE_PATH  . ' {entity?} {--all}';

    protected $description = 'Генерация "paths" для документация по спецификации jsonapi';

    public function handle()
    {
        if ($this->option('all')) {
            $this->generateAll();

            return;
        }

        $this->getEntity();
        $this->selectMode();

        $this->generateEntityPath();
        $this->generateEntityIdPath();
        $this->generateEntityRelatedPaths();
        $this->generateEntityRelationshipsPaths();

    }

    private function generateAll()
    {
        $entities = config('docs-generator.entity-settings');
        $this->selectMode();

        foreach ($entities as $entityClass => $configs) {
            if ($configs['without_paths'] ?? false) {
                continue;
            }

            $this->info("\n------- $entityClass -------\n");
            $this->input->setArgument('entity', new $entityClass());

            $this->runCommand(CommandNames::DOCS_GENERATE_PATH, $this->arguments(), $this->output);
        }
    }

    private function generateEntityPath()
    {
        $entityName = (new ReflectionClass($this->getEntity()))->getShortName();
        $paths = (new EntitiesPathGenerator())->generate($entityName);

        $dirPath = $this->ask("Название директории:", PathStorage::getPathsDir($entityName));
        $filename = $this->ask("Название файла:", PathStorage::getEntityPathsFilename($entityName));

        FileSaver::saveYamlFromArray($paths, $dirPath, $filename);
        $this->info("File {$filename} created successfully");
    }

    private function generateEntityIdPath()
    {
        $entityName = (new ReflectionClass($this->getEntity()))->getShortName();
        $paths = (new EntitiesIdPathGenerator())->generate($entityName);

        $dirPath = $this->ask("Название директории:", PathStorage::getPathsDir($entityName));
        $filename = $this->ask("Название файла:", PathStorage::getEntityPathsFilename($entityName, 'Id'));

        FileSaver::saveYamlFromArray($paths, $dirPath, $filename);
        $this->info("File {$filename} created successfully");
    }

    private function generateEntityRelatedPaths()
    {
        $parametersParser = new ParametersParser();
        $generator = new EntitiesIdRelatedPathGenerator($parametersParser);

        $className = get_class($this->getEntity());
        $entityName = (new ReflectionClass($this->getEntity()))->getShortName();

        $relationships = SettingsGetter::getSetting($className, 'include');

        foreach ($relationships as $relationship) {
            $content = $generator->generate($entityName, $relationship);

            $dirPath = $this->ask("Название директории:", PathStorage::getPathsRelatedDir($entityName));
            $filename = $this->ask("Название файла:", PathStorage::getEntityRelatedPathsFilename($entityName, Helper::capCamel($relationship)));

            FileSaver::saveYamlFromArray($content, $dirPath, $filename);
            $this->info("File {$filename} created successfully");
        }
    }

    private function generateEntityRelationshipsPaths()
    {
        $parametersParser = new ParametersParser();
        $generator = new EntitiesIdRelationshipsPathGenerator($parametersParser);

        $className = get_class($this->getEntity());
        $entityName = (new ReflectionClass($this->getEntity()))->getShortName();

        $relationships = SettingsGetter::getSetting($className, 'include');

        foreach ($relationships as $relationship) {

            $content = $generator->generate($entityName, $relationship);

            $dirPath = $this->ask("Название директории:", PathStorage::getPathsRelationshipsDir($entityName));
            $filename = $this->ask("Название файла:", PathStorage::getEntityRelationshipsPathsFilename($entityName, Helper::capCamel($relationship)));

            FileSaver::saveYamlFromArray($content, $dirPath, $filename);
            $this->info("File {$filename} created successfully");
        }
    }
}
