<?php

namespace Synergyhub\DocsGenerator\Commands\Generators\RequestBody;

use Illuminate\Console\Command;
use ReflectionClass;
use Synergyhub\DocsGenerator\Commands\Generators\AbstractGenerateCommand;
use Synergyhub\DocsGenerator\Enums\Actions;
use Synergyhub\DocsGenerator\Enums\CommandNames;
use Synergyhub\DocsGenerator\Generators\Parameters\EntityParametersGenerator;
use Synergyhub\DocsGenerator\Generators\RequestBody\RequestBodyGenerator;
use Synergyhub\DocsGenerator\Generators\RequestBody\RequestBodyRelationshipsGenerator;
use Synergyhub\DocsGenerator\Util\PathStorage;
use Synergyhub\DocsGenerator\Util\FileSaver;
use Synergyhub\DocsGenerator\Parsers\ModelsParser;

class RequestBodyGenerateCommand extends AbstractGenerateCommand
{
    /**
     * TestCamelCase
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = CommandNames::DOCS_GENERATE_REQUEST . ' {entity?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Генерация компонентов "request-body" для openapi документации';

    public function handle()
    {
        $entity = $this->getEntity();
        $entityName = (new ReflectionClass($entity))->getShortName();

        if ($this->confirm('Generate StoreRequest?', true)) {
            $this->generateRequest($entityName, Actions::STORE);
        }

        if ($this->confirm('Generate UpdateRequest?', true)) {
            $this->generateRequest($entityName, Actions::UPDATE);
        }

        if ($this->confirm('Generate RelationshipsRequest?', true)) {
            $this->generateRequestRelationships($entityName);
        }
    }


    private function generateRequest(string $entityName, string $action)
    {
        $data = (new RequestBodyGenerator())->generate($this->getEntity(), $action);
        $dirPath = $this->ask("Название директории:", PathStorage::getRequestBodiesDir($entityName));
        $filename = $this->ask("Название файла:", PathStorage::getRequestBodiesFilename($entityName, $action));

        FileSaver::saveYamlFromArray($data, $dirPath, $filename);
        $this->info("File {$filename} created successfully");
    }

    private function generateRequestRelationships(string $entityName)
    {
        try {
            $data = (new RequestBodyRelationshipsGenerator())->generate($this->getEntity());
            $dirPath = $this->ask("Название директории:", PathStorage::getRequestBodiesRelationshipsDir($entityName));
            $filename = $this->ask("Название файла:", PathStorage::getRequestBodiesRelationshipsFilename($entityName));

            FileSaver::saveYamlFromArray($data, $dirPath, $filename);
            $this->info("File {$filename} created successfully");
        } catch (\Exception $exception) {
            echo $exception->getMessage();

            return;
        }
    }

}
