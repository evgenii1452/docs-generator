<?php

namespace Synergyhub\DocsGenerator\Commands\Generators\Schema;

use ReflectionClass;
use Synergyhub\DocsGenerator\Commands\Generators\AbstractGenerateCommand;
use Synergyhub\DocsGenerator\Enums\CommandNames;
use Synergyhub\DocsGenerator\Generators\Schema\SchemaLinksGenerator;
use Synergyhub\DocsGenerator\Generators\Schema\SchemaRelationshipsGenerator;
use Synergyhub\DocsGenerator\Util\PathStorage;
use Synergyhub\DocsGenerator\Util\FileSaver;
use Synergyhub\DocsGenerator\Generators\Schema\SchemaGenerator;

class SchemaGenerateCommand extends AbstractGenerateCommand
{
    /**
     * TestCamelCase
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = CommandNames::DOCS_GENERATE_SCHEMA . ' {entity?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Генерация компонентов "schema" для openapi документации';

    public function handle()
    {
        $entity = $this->getEntity();
        $entityName = (new ReflectionClass($entity))->getShortName();

        if ($this->confirm("Create the Schema?", true)) {
            $this->generateSchema($entityName);
        }

        if ($this->confirm("Create the SchemaLinks?", true)) {
            $this->generateLinks($entityName);
        }

        if ($this->confirm("Create the SchemaRelationships?", true)) {
            $this->generateRelationships($entityName);
        }
    }

    private function generateSchema(string $entityName)
    {
        $data = (new SchemaGenerator($this))->generate($this->getEntity());
        $dirPath = $this->ask("Название директории:", PathStorage::getSchemasDir($entityName));
        $filename = $this->ask("Название файла:", PathStorage::getSchemaFilename($entityName));

        FileSaver::saveYamlFromArray($data, $dirPath, $filename);
        $this->info("File {$filename} created successfully");
    }

    private function generateLinks(string $entityName)
    {
        $data = (new SchemaLinksGenerator())->generate($this->getEntity());
        $dirPath = $this->ask("Название директории:", PathStorage::getSchemasDir($entityName));
        $filename = $this->ask("Название файла:", PathStorage::getSchemaLinksFilename($entityName));

        FileSaver::saveYamlFromArray($data, $dirPath, $filename);
        $this->info("File {$filename} created successfully");
    }

    private function generateRelationships(string $entityName)
    {
        $data = (new SchemaRelationshipsGenerator())->generate($this->getEntity());
        $dirPath = $this->ask("Название директории:", PathStorage::getSchemasDir($entityName));
        $filename = $this->ask("Название файла:", PathStorage::getSchemaRelationshipsFilename($entityName));

        FileSaver::saveYamlFromArray($data, $dirPath, $filename);
    }
}
