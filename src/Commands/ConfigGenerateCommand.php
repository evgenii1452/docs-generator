<?php

namespace Synergyhub\DocsGenerator\Commands;

use Illuminate\Support\Facades\DB;
use Illuminate\Console\Command;

class ConfigGenerateCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'gen:config';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Генерация конфига для кастомного описания полей'; //@todo


    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        DB::getDoctrineSchemaManager()->getDatabasePlatform()->registerDoctrineTypeMapping('enum', 'string');
    }

    public function handle()
    {
        $configPath = base_path('config/docs-generator/association/descriptions.php');

        if (!file_exists($configPath)) {
            throw new \Exception(
                'Not found config file. Please run command: php artisan vendor:publish --tag=docs-generator:configs'
            );
        }

        $template = file_get_contents(__DIR__ . '/../Templates/configs/descriptions.txt');

        $tables = DB::select('SHOW TABLES');

        $fields = [];

        foreach ($tables as $table) {
            $tableName = $table->Tables_in_api_mp_sections;
            $columns = \Schema::getColumnListing($tableName);

            foreach ($columns as $column) {
                $field = \DB::getDoctrineColumn($tableName, $column);

                $fields[$field->getName()] = $field->getName();

            }
        }

        $currentDescriptions = config('docs-generator.association.descriptions') ?? [];
        $fields = array_merge($fields, $currentDescriptions);

        $fieldsAsString = "[\n";

        foreach ($fields as $key => $field) {
            $fieldsAsString .= "    '{$key}' => '{$field}',\n";
        }

        $fieldsAsString .= "];";

        $content = sprintf($template, $fieldsAsString);

        file_put_contents($configPath, $content);

        $this->info('Success!');
    }
}
