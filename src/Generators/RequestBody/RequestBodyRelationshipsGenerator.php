<?php

namespace Synergyhub\DocsGenerator\Generators\RequestBody;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;
use ReflectionClass;
use Synergyhub\DocsGenerator\Enums\EntitySettingKeys;
use Synergyhub\DocsGenerator\Util\SettingsGetter;

class RequestBodyRelationshipsGenerator
{
    /**
     * @throws \Exception
     */
    public function generate(Model $entity): array
    {
        $className = get_class($entity);
        $entityName = (new ReflectionClass($entity))->getShortName();

        $entitySettings = SettingsGetter::getEntitySettings($className, [EntitySettingKeys::INCLUDE]);
        $relationships = [];

        foreach ($entitySettings[EntitySettingKeys::INCLUDE] as $relationName) {
            $relationName = Str::ucfirst(Str::camel($relationName));

            $relationships["{$entityName}Relationship{$relationName}RequestBody"] = $this->buildRelationship($relationName);
        }

        return $relationships;
    }

    private function buildRelationship(string $relationName): array
    {

        if ($relationName == Str::singular($relationName)) {
            $schemaName = sprintf("%sData", Str::singular($relationName));
        }

        if ($relationName != Str::singular($relationName)) {
            $schemaName = sprintf("%sListData", Str::singular($relationName));
        }

        return [
                'content' => [
                    'application/vnd.api+json' => [
                        "schema" => [
                            'type' => 'object',
                            'properties' => [
                                'data' => [
                                    '$ref' => "#/components/schemas/{$schemaName}"
                                ]
                            ]
                        ]
                    ]
                ]
            ];
    }

}
