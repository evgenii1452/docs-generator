<?php

namespace Synergyhub\DocsGenerator\Generators\RequestBody;

use Illuminate\Database\Eloquent\Model;
use ReflectionClass;
use Synergyhub\DocsGenerator\Enums\EntitySettingKeys;
use Synergyhub\DocsGenerator\Util\SettingsGetter;

class RequestBodyGenerator
{
    public function generate(Model $entity, string $action): array
    {
        $className = get_class($entity);
        $entityName = (new ReflectionClass($entity))->getShortName();

        $entitySettings = SettingsGetter::getEntitySettings($className);

        if (count($entitySettings[EntitySettingKeys::INCLUDE] ?? []) > 0) {
            $hasRelations = true;
        }

        return [
            "{$entityName}{$action}RequestBody" => [
                'content' => [
                    'application/vnd.api+json' => [
                        'schema' => $this->buildSchema($entityName, $hasRelations ?? false)
                    ]
                ]
            ]
        ];
    }

    private function buildSchema(string $entityName, bool $hasRelations)
    {
        $properties = [
            'type' => [
                '$ref' => "#/components/schemas/{$entityName}Type",
            ],
            'attributes' => [
                '$ref' => "#/components/schemas/{$entityName}Attributes",
            ],
        ];

        if ($hasRelations) {
            $properties['relationships'] = [
                    '$ref' => "#/components/schemas/{$entityName}RelationshipsData",
            ];
        }

        return [
            'type' => 'object',
            'properties' => [
                'data' => [
                    'type' => 'object',
                    'properties' => $properties
                ]
            ]
        ];
    }
}
