<?php

namespace Synergyhub\DocsGenerator\Generators\Response;

use Illuminate\Database\Eloquent\Model;
use ReflectionClass;
use Synergyhub\DocsGenerator\Enums\Actions;
use Synergyhub\DocsGenerator\Enums\EntitySettingKeys;
use Synergyhub\DocsGenerator\Util\Helper;
use Synergyhub\DocsGenerator\Util\SettingsGetter;

final class ResponseRelatedGenerator extends ResponseGenerator
{
    /**
     * @throws \Exception
     */
    public function __invoke(Model $entity): array
    {
        $className = get_class($entity);
        $entityName = (new ReflectionClass($entity))->getShortName();

        $entitySettings = SettingsGetter::getEntitySettings($className);

        $relations = $entitySettings[EntitySettingKeys::INCLUDE] ?? [];

        if (empty($relations)) {
            throw new \Exception("Warning: Not found \"include\" settings for {$entityName} \n");
        }

        $response = [];

        foreach ($relations as $relation) {
            $relationName = Helper::capCamel($relation);
            $key = "{$entityName}Related{$relationName}Response";

            $response[$key] = [
                'description' => 'OK',
                'headers' => $this->getDefaultHeaders(),
                'content' => [
                    'application/vnd.api+json' => [
                        'schema' => [
                            'type' => 'object',
                            'properties' => $this->getProperties($relation)
                        ]
                    ]
                ]
            ];

        }

        return $response;
    }

    /**
     * @param string $relation
     *
     * @return array
     */
    private function getProperties(string $relation): array
    {
        $action = ($relation == \Str::plural($relation)) ? Actions::LIST : Actions::DETAIL;
        $entityName = Helper::capCamel(\Str::singular($relation));

        $items = $this->buildItems($entityName);
        $links = $this->buildLinks($entityName, $action);

        if ($action == Actions::LIST) {
            return [
                'data' => [
                    'type' => 'array',
                    'items' => $items
                ],
                'links' => $links,
            ];
        }

        return [
            'data' => $items,
            'links' => $links,
        ];
    }
}
