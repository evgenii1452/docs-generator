<?php

namespace Synergyhub\DocsGenerator\Generators\Response;

use Illuminate\Database\Eloquent\Model;
use ReflectionClass;
use Synergyhub\DocsGenerator\Enums\EntitySettingKeys;
use Synergyhub\DocsGenerator\Util\Helper;
use Synergyhub\DocsGenerator\Util\SettingsGetter;

final class ResponseRelationshipsGenerator extends ResponseGenerator
{
    /**
     * @throws \Exception
     */
    public function __invoke(Model $entity): array
    {
        $className = get_class($entity);
        $entityName = (new ReflectionClass($entity))->getShortName();

        $entitySettings = SettingsGetter::getEntitySettings($className);

        $relations = $entitySettings[EntitySettingKeys::INCLUDE] ?? [];

        if (empty($relations)) {
            throw new \Exception("Warning: Not found \"include\" settings for {$entityName} \n");
        }

        $response = [];

        foreach ($relations as $relation) {
            $relationName = \Str::ucfirst(\Str::camel($relation));
            $key = "{$entityName}Relationship{$relationName}Response";

            $response[$key] = [
                'description' => 'OK',
                'headers' => $this->getDefaultHeaders(),
                'content' => [
                    'application/vnd.api+json' => [
                        'schema' => [
                            'type' => 'object',
                            'properties' => $this->getProperties($entityName, $relation)
                        ]
                    ]
                ]
            ];

        }

        return $response;
    }

    /**
     * @param string $entityName
     * @param string $relation
     *
     * @return array[]
     */
    private function getProperties(string $entityName, string $relation): array
    {

        if ($relation == \Str::singular($relation)) {
            $dataSchemaName = sprintf("#/components/schemas/%sData", Helper::capCamel(\Str::singular($relation)));
        } else {
            $dataSchemaName = sprintf("#/components/schemas/%sListData", Helper::capCamel(\Str::singular($relation)));
        }

        return [
            'data' => [
                '$ref' => $dataSchemaName
            ],
            'links' => [
                '$ref' => sprintf("#/components/schemas/%s%sLinks", Helper::capCamel($entityName), Helper::capCamel($relation))
            ],
        ];
    }
}
