<?php

namespace Synergyhub\DocsGenerator\Generators\Response;

use Illuminate\Database\Eloquent\Model;
use ReflectionClass;
use Synergyhub\DocsGenerator\Enums\Actions;

final class ResponseStoreGenerator extends ResponseGenerator
{
    public function __invoke(Model $entity): array
    {
        $entityName = (new ReflectionClass($entity))->getShortName();

        return [
            "{$entityName}StoreResponse" => [
                'description' => 'Created',
                'headers' => $this->getHeaders($entityName),
                'content' => [
                    'application/vnd.api+json' => [
                        'schema' => [
                            'type' => 'object',
                            'properties' => $this->getProperties($entityName)
                        ]
                    ]
                ]
            ]
        ];
    }

    private function getProperties(string $entityName): array
    {
        $items = $this->buildItems($entityName);
        unset($items['properties']['relationships']);

        return [
            'data' => $items,
            'links' => $this->buildLinks($entityName, Actions::DETAIL),
            'relationships' => $this->buildRelationships($entityName),
        ];
    }

    protected function getHeaders(string $entityName): array
    {
        $entityNameForPath = \Str::kebab(\Str::plural($entityName));
        $description = sprintf('%s/api/v1/%s/1', config('docs-generator.settings.domain'), $entityNameForPath);

        return [
            'Location' => [
                'schema' => [
                    'type' => 'string'
                ],
                'description' => $description
            ],
            'Content-Type' => [
                'schema' => [
                    'type' => 'string'
                ],
                'description' => 'application/vnd.api+json'
            ]
        ];

    }
}
