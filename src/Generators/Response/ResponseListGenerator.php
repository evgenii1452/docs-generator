<?php

namespace Synergyhub\DocsGenerator\Generators\Response;

use Illuminate\Database\Eloquent\Model;
use ReflectionClass;
use Synergyhub\DocsGenerator\Enums\Actions;
use Synergyhub\DocsGenerator\Enums\EntitySettingKeys;
use Synergyhub\DocsGenerator\Util\SettingsGetter;

final class ResponseListGenerator extends ResponseGenerator
{
    public function __invoke(Model $entity): array
    {
        $className = get_class($entity);
        $entityName = (new ReflectionClass($entity))->getShortName();

        $entitySettings = SettingsGetter::getEntitySettings($className);

        $relations = $entitySettings[EntitySettingKeys::INCLUDE] ?? [];

        return [
            "{$entityName}ListResponse" => [
                'description' => 'OK',
                'headers' => $this->getDefaultHeaders(),
                'content' => [
                    'application/vnd.api+json' => [
                        'schema' => [
                            'type' => 'object',
                            'properties' => $this->getProperties($entityName, $relations)
                        ]
                    ]
                ]
            ]
        ];
    }

    private function getProperties(string $entityName, array $relations): array
    {
        $properties = [
            'data' => [
                'type' => 'array',
                'items' => $this->buildItems($entityName)
            ],
            'links' => $this->buildLinks($entityName, Actions::LIST),
        ];

        if (!empty($relations)) {
            $properties['included'] = $this->buildInclude($relations);
        }

        return $properties;
    }
}
