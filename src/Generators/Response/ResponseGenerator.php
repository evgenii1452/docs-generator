<?php

namespace Synergyhub\DocsGenerator\Generators\Response;

use Illuminate\Database\Eloquent\Model;
use Synergyhub\DocsGenerator\Enums\ResponseTypes;
use Synergyhub\DocsGenerator\Util\Helper;

class ResponseGenerator
{
    /**
     * @param Model $entity
     * @param string $action
     * @return array
     * @throws \Exception
     */
    public function generate(Model $entity, string $action): array
    {
        switch ($action) {
            case ResponseTypes::LIST: {
                return (new ResponseListGenerator)($entity);
            }
            case ResponseTypes::DETAIL: {
                return (new ResponseDetailGenerator)($entity);
            }
            case ResponseTypes::STORE: {
                return (new ResponseStoreGenerator)($entity);
            }
            case ResponseTypes::RELATIONSHIPS: {
                return (new ResponseRelationshipsGenerator)($entity);
            }
            case ResponseTypes::RELATED: {
                return (new ResponseRelatedGenerator)($entity);
            }
        }

        throw new \Exception("Response type not found");
    }

    protected function getDefaultHeaders(): array
    {
        return [
            'Content-Type' => [
                'schema' => [
                    'type' => 'string'
                ],
                'description' => 'application/vnd.api+json'
            ]
        ];
    }

    protected function buildItems(string $entityName): array
    {
        $entityName = \Str::singular($entityName);

        return [
            'type' => 'object',
            'properties' => [
                'id' => [
                    '$ref' => "#/components/schemas/{$entityName}Id"
                ],
                'type' => [
                    '$ref' => "#/components/schemas/{$entityName}Type"
                ],
                'attributes' => [
                    '$ref' => "#/components/schemas/{$entityName}Attributes"
                ],
                'relationships' => [
                    '$ref' => "#/components/schemas/{$entityName}Relationships"
                ],
            ]
        ];
    }

    protected function buildLinks(string $prefix, string $postfix = ''): array
    {
        return [
            '$ref' => "#/components/schemas/{$prefix}Link{$postfix}"
        ];
    }


    protected function buildInclude(array $relations): array
    {
        $items = $this->getIncludeItems($relations);

        if (count($items) == 1) {
            return [
                'type' => 'array',
                'items' => array_shift($items)
            ];
        }

        return [
            'type' => 'array',
            'items' => [
                'anyOf' => $items
            ]
        ];
    }

    protected function getIncludeItems(array $relations): array
    {
        $items = [];

        foreach ($relations as $relation) {
            $relationName = \Str::singular(Helper::capCamel($relation));

            $items[] = [
                'type' => 'object',
                'properties' => [
                    'id' => [
                        '$ref' => "#/components/schemas/{$relationName}Id"
                    ],
                    'type' => [
                        '$ref' => "#/components/schemas/{$relationName}Type"
                    ],
                    'attributes' => [
                        '$ref' => "#/components/schemas/{$relationName}Attributes"
                    ],
                    'relationships' => [
                        '$ref' => "#/components/schemas/{$relationName}RelationshipsLinks"
                    ],
                ]
            ];
        }

        return $items;
    }

    protected function buildRelationships(string $entityName)
    {
        return [
            '$ref' => "#/components/schemas/{$entityName}RelationshipsLinks"
        ];
    }
}
