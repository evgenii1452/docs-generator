<?php

namespace Synergyhub\DocsGenerator\Generators\Parameters;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;
use ReflectionClass;
use Synergyhub\DocsGenerator\Util\SettingsGetter;

class FilterParametersGenerator
{
    public function generate(Model $entity): array
    {
        $className = get_class($entity);
        $entityName = (new ReflectionClass($entity))->getShortName();

        $entitySettings = SettingsGetter::getEntitySettings($className);

        if (empty($entitySettings['filters'])) {
            throw new \Exception("Warning: Filters for {$entityName} not found \n");
        }

        $filters = $entitySettings['filters'];

        return $this->getFilters($entityName, $filters);
    }

    private function getFilters(string $entityName, array $filters): array
    {

        $result = [];

        foreach ($filters as $filterName => $filter) {
            $camelFilterName = Str::ucfirst(Str::camel($filterName));

            $result["{$entityName}Filter{$camelFilterName}Parameter"] = [
                'in' => 'query',
                'name' => "filter[{$filterName}]",
                'schema' => [
                    'type' => $filter['type'] ?? 'string'
                ],
                'example' => 'example', //@todo
                'description' => $filter['description'] ?? $filterName //@todo
            ];

        }

        return $result;
    }
}
