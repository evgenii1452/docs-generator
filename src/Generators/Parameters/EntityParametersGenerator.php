<?php

namespace Synergyhub\DocsGenerator\Generators\Parameters;

use Illuminate\Database\Eloquent\Model;
use ReflectionClass;
use Synergyhub\DocsGenerator\Enums\EntitySettingKeys;
use Synergyhub\DocsGenerator\Util\Helper;
use Synergyhub\DocsGenerator\Util\SettingsGetter;

class EntityParametersGenerator
{
    public function generate(Model $entity): array
    {
        $className = get_class($entity);
        $entityName = (new ReflectionClass($entity))->getShortName();

        $entitySettings = SettingsGetter::getEntitySettings($className);

        $include = Helper::arrayValuesToString($entitySettings[EntitySettingKeys::INCLUDE] ?? []);
        $sort = $entitySettings[EntitySettingKeys::SORT] ?? [];

        $parameters = [];

        if (!empty($include)) {
            $parameters["{$entityName}IncludeParameter"] = $this->getInclude($include);
        }

        if (!empty($sort)) {
            $parameters["{$entityName}SortParameter"] = $this->getSort($sort);
        }

        $parameters["{$entityName}IdParameter"] = $this->getId();

        return $parameters;
    }

    private function getInclude(string $include): array
    {
        return [
            'in' => 'query',
            'name' => 'include',
            'schema' => [
                'type' => 'string',
            ],
            'example' => $include,
            'description' => 'Подключение отношений'
        ];
    }

    private function getSort(array $sort): array
    {

        return [
            'in' => 'query',
            'name' => 'sort',
            'schema' => [
                'type' => 'string',
                'enum' => $sort,
            ],
            'example' => 5,
            'description' => 'Парметр сортировки'
        ];
    }

    private function getId(): array
    {
        return [
            'in' => 'path',
            'name' => 'id',
            'schema' => [
                'type' => 'integer'
            ],
            'required' => true,
            'example' => 5,
            'description' => 'Идентификатор'
        ];
    }
}
