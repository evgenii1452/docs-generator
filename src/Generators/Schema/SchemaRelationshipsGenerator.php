<?php

namespace Synergyhub\DocsGenerator\Generators\Schema;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;
use ReflectionClass;
use Synergyhub\DocsGenerator\Enums\EntitySettingKeys;

class SchemaRelationshipsGenerator
{
    public function generate(Model $entity): array
    {
        $className = get_class($entity);
        $entityName = (new ReflectionClass($entity))->getShortName();

        $entitySettings = config('docs-generator.entity-settings.' . $className);

        if (empty($entitySettings) || empty($entitySettings[EntitySettingKeys::INCLUDE])) {
            printf("Warning: Not found \"include\" settings for {$entityName} \n");
        }

        $relations = $entitySettings[EntitySettingKeys::INCLUDE] ?? [];

        return array_merge(
            $this->getEntityData($entityName),
            $this->getEntityListData($entityName),
            $this->getEntityRelationLinks($entityName, $relations),
            $this->getEntityRelationshipsLinks($entityName, $relations),
            $this->getEntityRelationships($entityName, $relations),
            $this->getEntityRelationshipsData($entityName, $relations),
        );
    }

    private function getEntityData(string $entityName): array
    {
        return [
            "{$entityName}Data" => [
                'type' => 'object',
                'properties' => [
                    'id' => [
                        'type' => 'integer',
                        'example' => 1,
                        'description' => 'Идентификатор',
                    ],
                    'type' => [
                        'type' => 'string',
                        'example' => Str::kebab(Str::plural($entityName)),
                        'description' => 'Тип ресурса',
                    ]
                ]
            ]
        ];
    }

    private function getEntityListData(string $entityName): array
    {
        return [
            "{$entityName}ListData" => [
                'type' => 'array',
                'items' => [
                    '$ref' => "#/components/schemas/{$entityName}Data"
                ]
            ]
        ];
    }

    private function getEntityRelationLinks(string $entityName, array $relations): array
    {
        $links = [];
        foreach ($relations as $relation) {
            $relationName = Str::ucfirst(Str::camel($relation));

            $selfLink = sprintf(
                '%s/api/v1/%s/1/relationships/%s',
                config('docs-generator.settings.domain'),
                Str::kebab(Str::plural($entityName)),
                Str::kebab($relation)
            );
            $relatedLink = sprintf('%s/api/v1/%s/1/%s',
                config('docs-generator.settings.domain'),
                Str::kebab(Str::plural($entityName)),
                Str::kebab($relation)
            );

            $links["{$entityName}{$relationName}Links"] = [
                'type' => 'object',
                'properties' => [
                    'self' => [
                        'type' => 'string',
                        'example' => $selfLink,
                        'description' => 'Ссылка для получения отношения.'
                    ],
                    'related' => [
                        'type' => 'string',
                        'example' => $relatedLink,
                        'description' => 'Ссылка для получения отношения.'
                    ]
                ]
            ];
        }

        return $links;
    }

    private function getEntityRelationshipsLinks(string $entityName, array $relations): array
    {
        $properties = [];

        foreach ($relations as $relation) {
            $relationName = Str::ucfirst(Str::camel($relation));

            $properties[$relation] = [
                'type' => 'object',
                'properties' => [
                    'links' => [
                        '$ref' => "#/components/schemas/{$entityName}{$relationName}Links"
                    ]
                ]
            ];
        }

        return [
            "{$entityName}RelationshipsLinks" => [
                'type' => 'object',
                'properties' => $properties
            ]
        ];
    }

    private function getEntityRelationships(string $entityName, array $relations): array
    {
        $properties = [];

        foreach ($relations as $relation) {
            $relationName = Str::ucfirst(Str::camel($relation));

            $schemaName = Str::singular($relationName);

            if ($relation != Str::singular($relation)) {
                $schemaName .= 'List';
            }


            $properties[$relation] = [
                'type' => 'object',
                'properties' => [
                    'data' => [
                        '$ref' => "#/components/schemas/{$schemaName}Data"
                    ],
                    'links' => [
                        '$ref' => "#/components/schemas/{$entityName}{$relationName}Links"
                    ]
                ]
            ];
        }

        return [
            "{$entityName}Relationships" => [
                'type' => 'object',
                'properties' => $properties
            ]
        ];
    }

    private function getEntityRelationshipsData(string $entityName, array $relations)
    {
        $properties = [];

        foreach ($relations as $relation) {
            $relationName = Str::ucfirst(Str::camel($relation));
            $schemaName = Str::singular($relationName);

            $properties[$relation] = [
                'type' => 'object',
                'properties' => [
                    'data' => [
                        '$ref' => "#/components/schemas/{$schemaName}Data"
                    ]
                ]
            ];
        }

        return [
            "{$entityName}RelationshipsData" => [
                'type' => 'object',
                'properties' => $properties
            ]
        ];
    }
}
