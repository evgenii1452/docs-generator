<?php

namespace Synergyhub\DocsGenerator\Generators\Schema;

use Illuminate\Console\Command;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;
use ReflectionClass;
use Synergyhub\DocsGenerator\Util\PropertyGenerator;

class SchemaLinksGenerator
{
    public function generate(Model $entity): array
    {
        $entityName = (new ReflectionClass($entity))->getShortName();

        $entityUrl = Str::kebab(Str::plural($entityName));

        $linkList = sprintf('%s/api/v1/%s', config('docs-generator.settings.domain'), $entityUrl);
        $linkDetail = sprintf('%s/api/v1/%s/1', config('docs-generator.settings.domain'), $entityUrl);

        return [
            "{$entityName}LinkList" => $this->getLinkList($linkList),
            "{$entityName}LinkDetail" => $this->getLinkDetail($linkDetail)
        ];
    }

    private function getLinkList(string $link): array
    {
        return [
            "type" => "object",
            "properties" => [
                "self" => [
                    'type' => 'string',
                    'example' => $link,
                    'description' => 'Ссылка для получения списка'
                ]
            ]
        ];
    }

    private function getLinkDetail(string $link): array
    {
        return [
            "type" => "object",
            "properties" => [
                "self" => [
                    'type' => 'string',
                    'example' => $link,
                    'description' => 'Ссылка для получения определенной записи'
                ]
            ]
        ];
    }
}
