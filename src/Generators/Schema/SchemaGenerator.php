<?php

namespace Synergyhub\DocsGenerator\Generators\Schema;

use Illuminate\Console\Command;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;
use ReflectionClass;
use Synergyhub\DocsGenerator\Util\PropertyGenerator;

class SchemaGenerator
{
    private Command $cli;

    public function __construct(Command $cli)
    {
        $this->cli = $cli;
    }

    public function generate(Model $entity): array
    {
        $entityName = (new ReflectionClass($entity))->getShortName();

        return [
            "{$entityName}Id" => $this->getId(),
            "{$entityName}Type" => $this->getType($entityName),
            "{$entityName}Attributes" => $this->getAttributes($entity),
        ];
    }

    private function getAttributes(Model $entity): array
    {
        $propertyGenerator = new PropertyGenerator($this->cli, get_class($entity));
        $properties = $propertyGenerator->generate($entity->getTable());

        return [
            'type' => 'object',
            'properties' => $properties
        ];
    }

    private function getId(): array
    {
        return [
            'type' => 'integer',
            'example' => 5,
            'description' => 'Идентификатор'
        ];
    }

    private function getType(string $classname): array
    {
        $type = Str::kebab(Str::plural($classname));

        return [
            'type' => 'string',
            'example' => $this->cli->ask('type', $type),
            'description' => 'Тип'
        ];
    }
}
