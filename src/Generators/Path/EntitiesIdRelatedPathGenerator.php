<?php

namespace Synergyhub\DocsGenerator\Generators\Path;

use Synergyhub\DocsGenerator\Parsers\ParametersParser;
use Synergyhub\DocsGenerator\Util\ResponseStorage;

class EntitiesIdRelatedPathGenerator
{
    private ParametersParser $parametersParser;

    public function __construct(ParametersParser $parametersParser)
    {
        $this->parametersParser = $parametersParser;
    }

    public function generate(string $entityName, string $relationName): array
    {
        $result = [];
        $result['get'] = $this->methodGet($entityName, $relationName);

        return $result;
    }

    private function methodGet(string $entityName, string $relationName): array
    {
        $entityIdParameter = $this->parametersParser->getFirstParameterLike($entityName, "{$entityName}Id");
        $parameters = $this->parametersParser->prepareParameters([$entityIdParameter]);

        $suffix = 'Related' . ucfirst(\Str::camel($relationName));
        $responses = (new ResponseStorage())->getResponses([200, 406, 404, 500], $entityName, $suffix);

        return [
            'tags' => [
                $entityName
            ],
            'summary' => "Getting related {$relationName}",
            'parameters' => $parameters,
            'responses' => $responses
        ];
    }
}
