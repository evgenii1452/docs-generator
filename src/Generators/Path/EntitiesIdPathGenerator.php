<?php

namespace Synergyhub\DocsGenerator\Generators\Path;

use Synergyhub\DocsGenerator\Enums\Actions;
use Synergyhub\DocsGenerator\Parsers\ParametersParser;
use Synergyhub\DocsGenerator\Util\ResponseStorage;

class EntitiesIdPathGenerator
{
    private ResponseStorage $responseStorage;
    private ParametersParser $parametersParser;

    public function __construct()
    {
        $this->responseStorage = app()->make(ResponseStorage::class);
        $this->parametersParser = app()->make(ParametersParser::class);
    }

    public function generate(string $entityName): array
    {
        return [
            'patch' => $this->patchPath($entityName),
            'delete' => $this->deletePath($entityName),
            'get' => $this->getPath($entityName),
        ];
    }


    public function patchPath(string $entityName): array
    {
        $entityIdParameter = $this->parametersParser->getFirstParameterLike($entityName, "{$entityName}Id");
        $parameters = $this->parametersParser->prepareParameters([$entityIdParameter]);

        return [
            'tags' => [
                $entityName
            ],
            'summary' => "Update {$entityName}",
            'parameters' => $parameters,
            'requestBody' => [
                '$ref' => "#/components/requestBodies/{$entityName}UpdateRequestBody"
            ],
            'responses' => $this->responseStorage
                ->getResponses([204, 406, 415, 422, 404, 500], $entityName, Actions::UPDATE)
        ];
    }

    public function deletePath(string $entityName): array
    {
        $entityIdParameter = $this->parametersParser->getFirstParameterLike($entityName, "{$entityName}Id");
        $parameters = $this->parametersParser->prepareParameters([$entityIdParameter]);

        return [
            'tags' => [
                $entityName
            ],
            'summary' => "Delete {$entityName}",
            'parameters' => $parameters,
            'responses' => $this->responseStorage
                ->getResponses([204, 404, 500])
        ];
    }

    public function getPath(string $entityName): array
    {
        return [
            'tags' => [
                $entityName
            ],
            'summary' => "Get {$entityName} detail",
            'parameters' => [
                [
                    '$ref' => "#/components/parameters/{$entityName}IdParameter",
                ],
                [
                    '$ref' => "#/components/parameters/{$entityName}IncludeParameter",
                ]
            ],
            'responses' => $this->responseStorage
                ->getResponses([200, 404, 406, 500], $entityName, Actions::DETAIL)
        ];
    }
}
