<?php

namespace Synergyhub\DocsGenerator\Generators\Path;

use Synergyhub\DocsGenerator\Enums\Actions;
use Synergyhub\DocsGenerator\Parsers\ParametersParser;
use Synergyhub\DocsGenerator\Util\ResponseStorage;

class EntitiesPathGenerator
{
    private ResponseStorage $responseStorage;
    private ParametersParser $parametersParser;

    public function __construct()
    {
        $this->responseStorage = app()->make(ResponseStorage::class);
        $this->parametersParser = app()->make(ParametersParser::class);
    }

    public function generate(string $entityName)
    {
        return [
            'post' => $this->postPath($entityName),
            'get' => $this->getPath($entityName),
        ];
    }

    private function postPath(string $entityName): array
    {
        return [
            'tags' => [
                $entityName
            ],
            'summary' => "Create {$entityName}",
            'requestBody' => [
                '$ref' => "#/components/requestBodies/{$entityName}StoreRequestBody",
            ],
            'responses' => $this->responseStorage
                ->getResponses([201, 500, 422, 406, 415], $entityName, Actions::STORE)
        ];
    }

    private function getPath(string $entityName): array
    {
        $filterParameters = $this->parametersParser->getParametersLike($entityName, 'Filter');
        $includeParameters = $this->parametersParser->getParametersLike($entityName, 'Include');
        $sortParameters = $this->parametersParser->getParametersLike($entityName, 'Sort');
        $parameters = $this->parametersParser
            ->prepareParameters(array_merge($filterParameters, $includeParameters, $sortParameters));

        return [
            'tags' => [
                $entityName
            ],
            'summary' => "Get {$entityName} list",
            'parameters' => $parameters,
            'responses' => $this->responseStorage
                ->getResponses([200, 500, 406], $entityName, Actions::LIST)
        ];
    }
}
