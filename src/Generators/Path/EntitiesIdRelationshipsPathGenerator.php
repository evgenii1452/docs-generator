<?php

namespace Synergyhub\DocsGenerator\Generators\Path;


use Synergyhub\DocsGenerator\Parsers\ParametersParser;
use Synergyhub\DocsGenerator\Util\ResponseStorage;

class EntitiesIdRelationshipsPathGenerator
{
    private ParametersParser $parametersParser;

    public function __construct(ParametersParser $parametersParser)
    {
        $this->parametersParser = $parametersParser;
    }

    public function generate(string $entityName, string $relationName): array
    {
        $result = [];

        $result['get'] = $this->methodGet($entityName, $relationName);

        if ($relationName != \Str::singular($relationName)) {
            $result['patch'] = $this->methodPatch($entityName, $relationName);
        }

        return $result;
    }

    private function methodGet(string $entityName, string $relationName): array
    {
        $entityIdParameter = $this->parametersParser->getFirstParameterLike($entityName, "{$entityName}Id");
        $parameters = $this->parametersParser->prepareParameters([$entityIdParameter]);

        $suffix = 'Relationship' . ucfirst(\Str::camel($relationName));
        $responses = (new ResponseStorage())->getResponses([200, 406, 404, 500], $entityName, $suffix);

        return [
            'tags' => [
                $entityName
            ],
            'summary' => "Get relationship {$relationName}",
            "Getting relationships {$relationName}",
            'parameters' => $parameters,
            'responses' => $responses
        ];
    }

    private function methodPatch(string $entityName, string $relationName): array
    {
        $entityIdParameter = $this->parametersParser->getFirstParameterLike($entityName, "{$entityName}Id");
        $parameters = $this->parametersParser->prepareParameters([$entityIdParameter]);

        $suffix = 'Relationship' . ucfirst(\Str::camel($relationName));
        $responses = (new ResponseStorage())->getResponses([200, 406, 404, 415, 422, 500], $entityName, $suffix);

        return [
            'tags' => [
                $entityName
            ],
            'summary' => "Change relationship {$relationName}",
            'parameters' => $parameters,
            'requestBody' => [
                '$ref' => "#/components/requestBodies/{$entityName}{$suffix}RequestBody"
            ],
            'responses' => $responses
        ];
    }
}
