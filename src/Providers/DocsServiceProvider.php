<?php

namespace Synergyhub\DocsGenerator\Providers;

use Illuminate\Support\ServiceProvider;
use Synergyhub\DocsGenerator\Commands\Builders\OpenApiBuildCommand;
use Synergyhub\DocsGenerator\Commands\ConfigGenerateCommand;
use Synergyhub\DocsGenerator\Commands\DocsBuildCommand;
use Synergyhub\DocsGenerator\Commands\Generators\DocsGenerateCommand;
use Synergyhub\DocsGenerator\Commands\Generators\Parameters\EntityParametersGenerateCommand;
use Synergyhub\DocsGenerator\Commands\Generators\PathGenerateCommand;
use Synergyhub\DocsGenerator\Commands\Generators\RequestBody\RequestBodyGenerateCommand;
use Synergyhub\DocsGenerator\Commands\Generators\Response\ResponseGenerateCommand;
use Synergyhub\DocsGenerator\Commands\Generators\Schema\SchemaGenerateCommand;
use Synergyhub\DocsGenerator\Commands\InitCommand;

class DocsServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     */
    public function register(): void
    {

    }

    /**
     * Bootstrap any application services.
     */
    public function boot(): void
    {
            $this->registerCommands();
            $this->registerConfigs();
            $this->publishFiles();
            $this->loadRoutes();
    }

    private function registerCommands()
    {
        $this->commands([
            DocsGenerateCommand::class,
            DocsBuildCommand::class,
            InitCommand::class,

            SchemaGenerateCommand::class,
            EntityParametersGenerateCommand::class,
            RequestBodyGenerateCommand::class,
            ResponseGenerateCommand::class,

            ConfigGenerateCommand::class,
            OpenApiBuildCommand::class,

            PathGenerateCommand::class
        ]);
    }

    private function registerConfigs()
    {
        $configDir = __DIR__ . '/../../config/';

        $this->mergeConfigFrom($configDir . 'settings.php', 'docs-generator.settings');
        $this->mergeConfigFrom($configDir . 'entity-settings.php', 'docs-generator.entity-settings');
        $this->mergeConfigFrom($configDir . 'association/types.php', 'docs-generator.association.types');
        $this->mergeConfigFrom($configDir . 'association/descriptions.php', 'docs-generator.association.descriptions');
    }

    public function publishFiles()
    {
        $this->publishes([
            __DIR__ . '/../../config/entity-settings.php' => base_path('config/docs-generator/entity-settings.php'),
            __DIR__ . '/../../config/settings.php' => base_path('config/docs-generator/settings.php'),
            __DIR__ . '/../../config/association/descriptions.php' => base_path('config/docs-generator/association/descriptions.php'),
        ], 'docs-generator:configs');

        $this->publishes([
            __DIR__ . '/../../resources/docs' => config('docs-generator.settings.docs_path'),
            __DIR__ . '/../../resources/views/redoc' => base_path('resources/views/redoc'),
        ], 'docs-generator:docs');
    }

    public function loadRoutes()
    {
        $this->loadRoutesFrom(__DIR__ . '/../../routes/docs.php');
    }
}
