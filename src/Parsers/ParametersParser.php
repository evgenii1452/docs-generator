<?php

namespace Synergyhub\DocsGenerator\Parsers;

class ParametersParser
{
    const PARAMETERS_PATH = '/src/components/parameters/';

    /**
     * @param string $entityName
     *
     * @return array
     */
    public function getAllParameters(string $entityName): array
    {
        $parameters = [];

        $docsPath = config('docs-generator.settings.docs_path');
        $dirName = \Str::kebab($entityName);
        $parametersDir = sprintf('%s/%s/%s', $docsPath, self::PARAMETERS_PATH, $dirName);

        $files = array_diff(scandir($parametersDir), ['.', '..']);

        foreach ($files as $filename) {
            $test = yaml_parse_file(sprintf('%s/%s', $parametersDir, $filename));
            $parameters = array_merge($parameters, array_keys($test));
        }

        return $parameters;
    }

    /**
     * @param string $entityName
     * @param string $like
     *
     * @return array
     */
    public function getParametersLike(string $entityName, string $like): array
    {
        $parameters = $this->getAllParameters($entityName);

        return array_filter($parameters, function ($parameterName) use ($like) {
            return str_contains($parameterName, $like);
        });
    }

    /**
     * @param string $entityName
     * @param string $like
     *
     * @return string
     *
     * @throws \Exception
     */
    public function getFirstParameterLike(string $entityName, string $like): string
    {
        $parameters = $this->getAllParameters($entityName);

        foreach ($parameters as $parameterName) {
            if (str_contains($parameterName, $like)) {
                return $parameterName;
            }
        }

        throw new \Exception(sprintf('Not found parameter: %s', $like));
    }

    public function prepareParameters(array $parameters): array
    {
        $result = [];

        foreach ($parameters as $parameter) {
            $result[] = [
                '$ref' => "#/components/parameters/{$parameter}"
            ];
        }

        return $result;
    }
}
