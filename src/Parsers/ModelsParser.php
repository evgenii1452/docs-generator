<?php

namespace Synergyhub\DocsGenerator\Parsers;


class ModelsParser
{

    public function getModels(string $basePath = 'app/Models/', bool $recursive = true): array
    {
        $baseNamespace = $this->prepareNamespaceFromPath($basePath);
        $files = scandir(base_path($basePath));

        $models = [];


        foreach ($files as $file) {
            if (str_contains($file, '.php')) {
                $className = str_replace('.php', '', $file);
                $models[$className] = $baseNamespace . $className;

                continue;
            }

            if (!$recursive) {
                continue;
            }

            $path = $basePath . $file;

            if (!in_array($file, ['.', '..']) && is_dir(base_path($path))) {
                $models = array_merge($models, $this->getModels($path, $recursive));
            }
        }

        return $models;
    }

    private function prepareNamespaceFromPath(string $path): string
    {
        $dirs = explode('/', $path);

        foreach ($dirs as $key => $dir) {
            if (empty($dir)) {
                unset($dirs[$key]);

                continue;
            }

            $dirs[$key] = ucfirst($dir);
        }

        $namespace = implode('\\', $dirs);

        return sprintf('%s\\', $namespace);
    }
}
