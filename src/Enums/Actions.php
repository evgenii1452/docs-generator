<?php

namespace Synergyhub\DocsGenerator\Enums;

class Actions
{
    const STORE = 'Store';
    const UPDATE = 'Update';
    const LIST = 'List';
    const DETAIL = 'Detail';
}
