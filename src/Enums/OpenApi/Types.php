<?php

namespace Synergyhub\DocsGenerator\Enums\OpenApi;

class Types
{
    const STRING = 'string';
    const INTEGER = 'integer';
    const NUMBER = 'number';
    const FLOAT = 'float';
    const BOOLEAN = 'boolean';
    const ARRAY = 'array';
    const OBJECT = 'object';
}
