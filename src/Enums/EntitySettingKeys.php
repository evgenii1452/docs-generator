<?php

namespace Synergyhub\DocsGenerator\Enums;

class EntitySettingKeys
{
    /**
     * Параметр для определения связей
     * Пример: ['organization', 'products', ...]
     */
    const INCLUDE = 'include';

    /**
     * Параметр для определения параметров сортировки
     * Пример: ['id', '-id', ...]
     */
    const SORT = 'sort';

    /**
     * Параметр для определения допустмых фильтров
     * Пример: [
     *      'organization_id' => ['type' => 'integer'],
     *      ...
     * ]
     */
    const FILTERS = 'filters';

    /**
     * Отключения автоматического генерирования путей для сущности
     * По умолчанию: false
     *
     * Пример: true/false
     */
    const WITHOUT_PATH = 'without_paths';
}
