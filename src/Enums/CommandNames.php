<?php

namespace Synergyhub\DocsGenerator\Enums;


class CommandNames
{
    const DOCS_GENERATE_BUILD = 'docs-generate:build';

    const DOCS_GENERATE_COMPONENTS = 'docs-generate:components';

    const DOCS_GENERATE_PATH = 'docs-generate:path';

    const DOCS_GENERATE_SCHEMA = 'docs-generate:schema';
    const DOCS_GENERATE_PARAMS = 'docs-generate:params';
    const DOCS_GENERATE_REQUEST = 'docs-generate:request';
    const DOCS_GENERATE_RESPONSE = 'docs-generate:response';
}
