<?php

namespace Synergyhub\DocsGenerator\Enums;

class ResponseTypes
{
    const ALL = 'All';
    const STORE = 'Store';
    const LIST = 'List';
    const DETAIL = 'Detail';
    const RELATIONSHIPS = 'Relationships';
    const RELATED = 'Related';
    const SKIP = 'Skip';
}
