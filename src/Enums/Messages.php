<?php

namespace Synergyhub\DocsGenerator\Enums;

class Messages
{
    const SELECT_MESSAGE = 'Select an action';
    const ENTER_MODEL = 'Enter model name';
    const DIR_NAME = 'Enter dir name';
    const FILE_NAME = 'Enter file name';
}
