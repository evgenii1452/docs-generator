<?php

namespace Synergyhub\DocsGenerator\Util;

use Symfony\Component\Yaml\Yaml;

class FileSaver
{
    public static function saveYamlFromArray(array $content, string $dirPath, string $filename): void
    {
        if (!str_contains($filename, '.yaml') || !str_contains($filename, '.yaml')) {
            $filename .= '.yaml';
        }

        $filepath = $dirPath . $filename;

        if (!file_exists($dirPath)) {
            mkdir($dirPath, 0777, true);
        }

        $result = yaml_emit($content, YAML_UTF8_ENCODING);

        $result = str_replace("---\n", '', $result);
        $result = str_replace("...\n", '', $result);

        file_put_contents($filepath, $result);
        chmod($dirPath, 0777);

        self::updateBuild($filepath);
    }

    public static function save(array $content, string $dirPath, string $filename): void
    {
        $filepath = $dirPath . $filename;

        if (!file_exists($dirPath)) {
            mkdir($dirPath, 0777, true);
        }

        file_put_contents($filepath, $content);
        chmod($dirPath, 0777);
    }

    public static function updateBuild(string $filepath): void
    {
        $buildFilePath = config('docs-generator.settings.docs_path') . '/src/build.yaml';
        $buildContent = file_get_contents($buildFilePath);

        $componentPath = explode('src/', $filepath)[1];

        if (!$buildContent) {
            exit('File build.yaml not found');
        }

        $buildContent = Yaml::parse($buildContent, Yaml::DUMP_OBJECT);

        if (Helper::isPath($filepath)) {
            $buildContent = self::updatePaths($componentPath, $buildContent);
        }

        if (Helper::isComponent($filepath)) {
            $buildContent = self::updateComponents($componentPath, $buildContent);
        }

        $result = yaml_emit($buildContent, YAML_UTF8_ENCODING);

        $result = str_replace("---\n", '', $result);
        $result = str_replace("...\n", '', $result);

        file_put_contents($buildFilePath, $result);
        chmod($buildFilePath, 0777);
    }

    public static function updateComponents(string $componentPath, array $buildContent): array
    {
        $type = Helper::getComponentType($componentPath);

        $components = $buildContent['components'][$type] ?? [];

        foreach ($components as $component) {
            if ($component == $componentPath) {
                return $buildContent;
            }
        }

        $components[] = $componentPath;

        $buildContent['components'][$type] = $components;

        return $buildContent;
    }

    public static function updatePaths(string $currentPath, array $buildContent): array
    {
        $path1 = self::preparePath($currentPath);
        $paths = $buildContent['paths'] ?? [];

        foreach ($paths as $path) {
            if ($path == $currentPath) {
                return $buildContent;
            }
        }

        $paths[$path1] = $currentPath;

        $buildContent['paths'] = $paths;

        return $buildContent;
    }

    private static function preparePath(string $path): string
    {
        $pathParts = explode('/', $path);

        $filename = array_pop($pathParts);

        $path = str_replace([
            '-path.yaml',
            '-id',
            '-related-',
            '-relationships-',
        ], [
            '',
            '/{id}/',
            '/{id}/',
            '/{id}/relationships/',
        ], \Str::kebab($filename));

        return sprintf('/%s', $path);
    }
}
