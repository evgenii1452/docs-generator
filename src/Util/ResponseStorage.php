<?php

namespace Synergyhub\DocsGenerator\Util;

class ResponseStorage
{
    const RESPONSES = [
        '200' => [
            '$ref' => '#/components/responses/%s%sResponse'
        ],
        '201' => [
            '$ref' => '#/components/responses/%s%sResponse'
        ],
        '204' => [
            '$ref' => '#/components/responses/NoContent'
        ],
        '404' => [
            '$ref' => '#/components/responses/NotFound'
        ],
        '406' => [
            '$ref' => '#/components/responses/NotAcceptable'
        ],
        '415' => [
            '$ref' => '#/components/responses/UnsupportedMediaType'
        ],
        '422' => [
            '$ref' => '#/components/responses/UnprocessableContent'
        ],
        '500' => [
            '$ref' => '#/components/responses/InternalServerError'
        ]
    ];

    public function getResponses(array $codes, string $entityName = '', string $suffix = '')
    {
        $responses = array_filter(self::RESPONSES, function ($code) use ($codes) {
            return in_array($code, $codes);
        }, ARRAY_FILTER_USE_KEY);

        array_walk($responses, function (&$response, $code) use ($entityName, $suffix) {
            if ($this->isSuccessResponse($code)) {
                $response = $this->prepareSuccessResponse($code, $entityName, $suffix);
            }
        });

        return $responses;
    }

    private function isSuccessResponse(string $code): bool
    {
        return in_array($code, ['200', '201']);
    }

    private function prepareSuccessResponse(string $code, string $entityName, string $action): array
    {
        return [
            '$ref' => sprintf(self::RESPONSES[$code]['$ref'], $entityName, $action)
        ];
    }
}
