<?php

namespace Synergyhub\DocsGenerator\Util;

use Synergyhub\DocsGenerator\Enums\EntitySettingKeys;

class SettingsGetter
{
    /**
     * @param string|null $entity
     *
     * @return array
     *
     * @throws \Exception
     */
    public static function getAllEntitySettings(?string $entity = null): array
    {
        if (is_null($entity)) {
            return config('docs-generator.entity-settings');
        }

        $settings = config('docs-generator.entity-settings.' . $entity);

        if (empty($settings)) {
            throw new \Exception(sprintf("Settings for %s not found", $entity));
        }

        if (empty($settings[EntitySettingKeys::INCLUDE]) &&
            empty($settings[EntitySettingKeys::SORT]) &&
            empty($settings[EntitySettingKeys::FILTERS])
        ) {
            throw new \Exception(sprintf("Settings for %s is empty", $entity));
        }

        return $settings;
    }

    /**
     * @param string $entity
     * @param array|null $required
     *
     * @return array
     *
     * @throws \Exception
     */
    public static function getEntitySettings(string $entity, ?array $required = null): array
    {
        $settings = config('docs-generator.entity-settings.' . $entity);

        if (empty($settings)) {
            throw new \Exception(sprintf("Settings for %s not found \n", $entity));
        }

        if (empty($settings[EntitySettingKeys::INCLUDE]) &&
            empty($settings[EntitySettingKeys::SORT]) &&
            empty($settings[EntitySettingKeys::FILTERS])
        ) {
            throw new \Exception(sprintf("Settings for %s is empty \n", $entity));
        }

        if (is_null($required)) {
            return $settings;
        }

        foreach ($required as $item) {
            if (empty($settings[$item])) {
                throw new \Exception(sprintf("%s is empty \n", $item));
            }
        }

        return $settings;
    }

    /**
     * @param string $entity
     * @param string $settingName enum: 'include', 'filters', 'sort'
     * @param bool $strict
     *
     * @return array
     *
     * @throws \Exception
     */
    public static function getSetting(string $entity, string $settingName, bool $strict = true): array
    {
        $settings = config('docs-generator.entity-settings.' . $entity);

        if (empty($settings)) {
            throw new \Exception(sprintf("Settings for %s not found", $entity));
        }

        if (empty($settings[$settingName]) && $strict) {
            throw new \Exception(sprintf("Settings for %s is empty", $entity));
        }

        return $settings[$settingName] ?? [];
    }
}
