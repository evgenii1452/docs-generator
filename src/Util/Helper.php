<?php

namespace Synergyhub\DocsGenerator\Util;

class Helper
{
    public static function arrayValuesToString(array|string $array): string
    {
        if (is_string($array)) {
            return $array;
        }

        return implode(',', $array);
    }

    public static function buildDataRef(string $prefix, string $postfix = '')
    {
        return [
            '$ref' => sprintf("#/components/schemas/%sData%s", self::capCamel($prefix), self::capCamel($postfix))
        ];
    }

    public static function capCamel(string $string): string
    {
        return \Str::ucfirst(\Str::camel($string));
    }

    public static function getComponentType(string $filepath): string
    {
        if (str_contains($filepath, 'components/schemas')) {
            return 'schemas';
        }

        if (str_contains($filepath, 'components/responses')) {
            return 'responses';
        }

        if (str_contains($filepath, 'components/parameters')) {
            return 'parameters';
        }

        if (str_contains($filepath, 'components/requestBodies')) {
            return 'requestBodies';
        }

        throw new \Exception('Not found component type.');
    }

    public static function isPath(string $filepath): bool
    {
        return str_contains($filepath, '/paths/');
    }

    public static function isComponent(string $filepath): bool
    {
        return str_contains($filepath, '/components/');
    }
}
