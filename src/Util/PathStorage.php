<?php

namespace Synergyhub\DocsGenerator\Util;

use Illuminate\Support\Str;

class PathStorage
{
    const SCHEMAS_DIR = '/src/components/schemas';
    const PARAMETERS_DIR = '/src/components/parameters';
    const REQUEST_BODIES_DIR = '/src/components/requestBodies';
    const RESPONSES_DIR = '/src/components/responses';
    const PATHS_DIR = '/src/paths';

    public static function getSchemasDir(string $entityName): string
    {
        $preparedEntityName = Str::kebab($entityName);


        return sprintf('%s/%s/', self::docsPath(self::SCHEMAS_DIR), $preparedEntityName);
    }

    public static function getParametersDir(string $entityName): string
    {
        $preparedEntityName = Str::kebab($entityName);

        return sprintf('%s/%s/', self::docsPath(self::PARAMETERS_DIR), $preparedEntityName);
    }

    public static function getPathsDir(string $entityName): string
    {
        $preparedEntityName = Str::kebab($entityName);

        return sprintf('%s/%s/', self::docsPath(self::PATHS_DIR), $preparedEntityName);
    }

    public static function getPathsRelationshipsDir(string $entityName): string
    {
        $preparedEntityName = Str::kebab($entityName);

        return sprintf('%s/%s/relationships/', self::docsPath(self::PATHS_DIR), $preparedEntityName);
    }

    public static function getPathsRelatedDir(string $entityName): string
    {
        $preparedEntityName = Str::kebab($entityName);

        return sprintf('%s/%s/related/', self::docsPath(self::PATHS_DIR), $preparedEntityName);
    }

    public static function getRequestBodiesDir(string $entityName): string
    {
        $preparedEntityName = Str::kebab($entityName);

        return sprintf('%s/%s/', self::docsPath(self::REQUEST_BODIES_DIR), $preparedEntityName);
    }

    public static function getRequestBodiesRelationshipsDir(string $entityName): string
    {
        return sprintf('%srelations/', self::getRequestBodiesDir($entityName));
    }

    public static function getResponseDir(string $entityName): string
    {
        $preparedEntityName = Str::kebab($entityName);

        return sprintf('%s/%s/', self::docsPath(self::RESPONSES_DIR), $preparedEntityName);
    }

    public static function getResponseRelatedDir(string $entityName): string
    {
        return sprintf('%srelated/', self::getResponseDir($entityName));
    }

    public static function getSchemaFilename(string $entityName): string
    {
        return sprintf("%sSchema.yaml", $entityName);
    }

    public static function getSchemaLinksFilename(string $entityName): string
    {
        return sprintf("%sLinksSchema.yaml", $entityName);

    }

    public static function getSchemaRelationshipsFilename(string $entityName): string
    {
        return sprintf("%sRelationshipsSchema.yaml", $entityName);
    }

    public static function getEntityParametersFilename(string $entityName): string
    {
        return sprintf("%sParameters.yaml", $entityName);
    }

    public static function getEntityPathsFilename(string $entityName, string $postfix = ''): string
    {
        $entityName = Str::plural($entityName);

        return sprintf("%s%sPath.yaml", $entityName, $postfix);
    }

    public static function getEntityRelationshipsPathsFilename(string $entityName, string $postfix = ''): string
    {
        $entityName = Str::plural($entityName);

        return sprintf("%sRelationships%sPath.yaml", $entityName, $postfix);
    }

    public static function getEntityRelatedPathsFilename(string $entityName, string $postfix = ''): string
    {
        $entityName = Str::plural($entityName);

        return sprintf("%sRelated%sPath.yaml", $entityName, $postfix);
    }

    public static function getEntityFilterParametersFilename(string $entityName): string
    {
        return sprintf("%sFilterParameters.yaml", $entityName);
    }

    public static function getRequestBodiesFilename(string $entityName, string $action): string
    {
        return sprintf("%s%sRequestBody.yaml", $entityName, $action);
    }

    public static function getRequestBodiesRelationshipsFilename(string $entityName): string
    {
        return sprintf("%sRelationshipsRequestBodies", $entityName);
    }

    public static function getResponseFilename(string $entityName, string $action): string
    {
        return sprintf("%s%sResponse.yaml", $entityName, $action);
    }

    public static function getResponseRelationshipsFilename(string $entityName, string $suffix): string
    {
        return sprintf("%s%sResponses.yaml", $entityName, $suffix);
    }

    public static function docsPath(string $path = ''):string
    {
        return config('docs-generator.settings.docs_path') . $path;
    }
}
