<?php

namespace Synergyhub\DocsGenerator\Util;


use DB;
use Doctrine\DBAL\Schema\Column;
use Doctrine\DBAL\Types\Types as DBTypes;
use Exception;
use Faker\Factory;
use Faker\Generator;
use Illuminate\Console\Command;
use Illuminate\Support\Carbon;
use Schema;
use Synergyhub\DocsGenerator\Enums\OpenApi\Types as SwaggerTypes;

class PropertyGenerator
{
    private Command $cli;
    private Generator $faker;
    private array $exceptFields = [
        'id',
//        'created_at',
//        'updated_at',
        'deleted_at'
    ];

    public function __construct(Command $cli, string $entityClass)
    {
        $this->cli = $cli;

        if ($entityClass::query()->count() > 0) {
            $this->entity = $entityClass::query()->get()->random();
        } else {
            $this->entity = new $entityClass();
        }

        $this->faker = Factory::create();
    }

    public function generate(string $table, bool $interactive = false): array
    {
        $properties = [];

        $columns = Schema::getColumnListing($table);

        foreach ($columns as $column) {
            $field = DB::getDoctrineColumn($table, $column);

            if (in_array($field->getName(), $this->exceptFields)) {
                continue;
            }

            $type = $field->getType()->getName();
            $name = $field->getName();

            if ($interactive) {
                $properties[$field->getName()] = [
                    "type" => $this->typeAssociation($type),
                    "example" => $this->cli->ask("Example for field:{$name}", $this->getExample($field)),
                    "description" => $this->cli->ask("Description for field:{$name}", $this->descriptionAssociation($field->getName())),
                ];
                continue;
            }

            $properties[$field->getName()] = [
                "type" => $this->typeAssociation($type),
                "example" => $this->getExample($field),
                "description" => $this->descriptionAssociation($field->getName()),
            ];
        }

        return $properties;
    }

    private function typeAssociation(string $type): string
    {
        $associations = config('docs-generator.association.types');

        foreach ($associations as $openapiType => $association) {
            if (in_array($type, $association)) {
                return $openapiType;
            }
        }

        throw new Exception("Unknown type" . $type);
    }

    private function descriptionAssociation(string $fieldName): string
    {
        $associations = config('docs-generator.association.descriptions');

        if (key_exists($fieldName, $associations)) {
            return $associations[$fieldName];
        }

        return $fieldName;
    }

    private function getExampleByType(string $type)
    {
        $openapiType = $this->typeAssociation($type);

        if ($this->isDateTime($type)) {
            return $this->faker->dateTime->format('Y-m-d h:i:s');
        }

        if ($openapiType == SwaggerTypes::STRING) {
            return $this->faker->slug;
        }

        if ($openapiType == SwaggerTypes::INTEGER) {
            return $this->faker->numberBetween(1, 10000);
        }

        if ($openapiType == SwaggerTypes::NUMBER) {
            return $this->faker->randomFloat(2, 100, 10000);
        }

        if ($openapiType == SwaggerTypes::FLOAT) {
            return $this->faker->randomFloat();
        }

        if ($openapiType == SwaggerTypes::BOOLEAN) {
            return (string)$this->faker->boolean;
        }

        if ($openapiType == SwaggerTypes::ARRAY || $openapiType == SwaggerTypes::OBJECT) {
            return $this->faker->rgbColorAsArray();
        }

        if ($openapiType == 'json') {
            return json_encode($this->faker->rgbColorAsArray());
        }

        throw new Exception("Unknown type " . $type);
    }

    private function isDateTime(string $type): bool
    {
        $datatimeTypes = [
            DBTypes::DATEINTERVAL,
            DBTypes::DATETIME_MUTABLE,
            DBTypes::DATETIME_IMMUTABLE,
            DBTypes::DATETIMETZ_MUTABLE ,
            DBTypes::DATETIMETZ_IMMUTABLE,
        ];

        if (in_array($type, $datatimeTypes)) {
            return true;
        }

        return false;
    }

    private function getExample(Column $field)
    {
        $type = $field->getType()->getName();
        $name = $field->getName();

        if (is_null($example = $this->entity->{$name})) {
            return $this->getExampleByType($type);
        }

        if ($example instanceof Carbon) {
            return $example->toDateTimeString();
        }


        if (is_object($example)) {
            return $example->toString();
        }

        return $example;
    }
}
