# Docs-generator

+ [Описание](#description);
+ [Установка](#installation);
+ [Основные команды](#main_commands);
+ [Команды для генерации отдельных компонентов](#other_commands);
+ [Использование](#usage);

## <a name="description"></a>Описание
<p>
    Пакет добавляет консольные команды 
    для генерации документации OpenApi в yaml формате
</p>
<p>
    <b> * Пакет работает только с фрейворком Laravel </b>
</p>

## <a name="installation"></a> Установка
1. Установить пакет с помощью менеджера зависимостей composer
```shell
composer require synergyhub/docs-generator:dev-master --dev
```
2. Если не используется auto-discovery, то необходимо зарегистрировать сервис провайдер 'Synergyhub\DocsGenerator\Providers\DocsServiceProvider' в config/app.php
```php
return [
//  other configs
    'providers' => [
//      other providers
        \Synergyhub\DocsGenerator\Providers\DocsServiceProvider::class
    ],
];
```
3. Запустить команду для создания конфиг файлов (configs/docs-generator/...)
```shell
$ php artisan vendor:publish --tag=docs-generator:configs
```
4. Заполнить конфиги. 
5. Запустить команду для создания директории, в которой будет находиться документация.
```shell
$ php artisan vendor:publish --tag=docs-generator:docs
```
После выполнения этих шагов будут доступны консольные команды для генерирования документации.

## <a name="main_commands"></a> Основные команды
#### Генерация компонентов для документации по спецификации jsonapi
```shell
$ php artisan docs-generate:components
```
- При запуске команды будет предложено выбрать модель для которой необходимо построить документацию.
- Для корректной работы необходимо заполнить настройки для модели в файле <b>config/docs-generator/entity-settings.php</b>

<hr>

#### Генерация компонентов для документации для сущностей для которых есть конфиги в файле configs/docs-generator/entity-settings.php

```shell
$ php artisan docs-generate:components --all
```
<hr>

#### Генерация маршрутов для сущностей у которых описаны конфиги в файле configs/docs-generator/entity-settings.php

```shell
$ php artisan docs-generate:path --all
```
<hr>

#### Сборка документации в файл .../src/openapi.yaml
```shell
$ php artisan docs-generate:build
```

## <a name="other_commands"></a> Команды для генерации отдельных компонентов

#### Генерация компонентов parameters для openapi документации
```shell
$ php artisan docs-generate:params
```
<hr>

#### Генерация компонентов request-body для openapi документации
```shell
$ php artisan docs-generate:request
```
<hr>

#### Генерация компонентов request для openapi документации
```shell
$ php artisan docs-generate:response
```
<hr>

#### Генерация компонентов schema для openapi документации
```shell
$ php artisan docs-generate:schema
```

## <a name="usage"></a> Использование

1. Необходимо установить пакет в проект
2. Заполнить конфигурационный файл (entity-settings.php)
3. Выполнить команды:
```shell
$ php artisan docs-generate:components --all
```
```shell
$ php artisan docs-generate:path --all
```
```shell
$ php artisan docs-generate:build
```
4. После этого необходимо создать два маршрута:
   1. Для файла документации
   2. Для отображения графического интерфейса документации

Пример:
```php
$configs = config('docs-generator.settings');

Route::get($configs['docs_url'], function () use ($configs) {
    return view('redoc.spec');
})->name('docs-generator.spec');

Route::get($configs['document_url'], function () use ($configs) {
    return File::get(sprintf('%s/openapi.yaml', $configs['docs_path']));
})->name('docs-generator.spec.document');```
```

После этого можно открыть документацию в браузере <br>
по пути: <b>https://{ваш_домен}/{путь_который_вы_укзали_в_settings.php}</b>

Пример: <b>https://example.ru/api/v1/docs/spec</b>
